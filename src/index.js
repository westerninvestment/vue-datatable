import Datatable from './components/Datatable'
import DatatableColumn from './components/DatatableColumn'

export const config = {}

export default {
  install: (Vue, options = {}) => {
    config.lang = options.lang || {}

    Vue.component('wi-table', Datatable)
    Vue.component('wi-table-column', DatatableColumn)
  }
}
