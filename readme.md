###Description
___

vue-datatable is a package for creating datatable with filter and pagination options.  
Package is using Font Awesome and lodash dependencies.

###Installation
___

`npm install @western-investment/vue-datatable`

###Usage 
___

* Import plugin and use it by calling `Vue.use()` global method.

```javascript
import Datatable from '@western-investment/vue-datatable'

Vue.use(Datatable)
```

**note**

As a second parameter, pass object, which is used for translating specific fields of datatable.  
If second parameter is not passed, default property values are shown.

```javascript
Vue.use(Datatable, {
    lang: {
        searchText: __('Search'),
        filterText: __('Filter'),
        clearFiltersText: __('Clear filters'),
        nextText: __('Next'),
        prevText: __('Previous')
    }
})
```

* Use plugin as component. `:fetch-callback` is required and must be a function which returns promise.  
`<wi-table-column field="...">` is required for data to be shown.

```javascript
<wi-table
    :fetch-callback="fetch"
>
    <wi-table-column field="id">Id</wi-table-column>
    <wi-table-column field="subject">Subject</wi-table-column>
</wi-table>
```

**note**

Promise return structure example:

```javascript
   data: [
       {
         id: '1',
         subject: 'First'
       },
       {
         id: '2',
         subject: 'Second'
       }
     ]
```
    
###Datatable
___

Datatable plugin consists of these parts: 

1. Table frame
2. Header
3. Table columns
4. Pagination

####Table

| Prop | Types | Default | Description |
|---|---|---|---|
| *fetchCallback | Function | - | must be function which returns promise. Fills datatable body with data. |
| disableSearch | Boolean | False | show or hide search field |
| disableHeader | Boolean | False | show or hide table header |
| disablePagination | Boolean | False | show or hide pagination |
| show-select | Boolean | False | show or hide *select all* column |

*is required

####Header

Use `slot="filtersForm"`, to add your preferred filters for datatable. Header also shows _clear filters_ and _filter_ buttons once slot is added. 

```
<wi-table :fetch-callback="fetch">
    <div slot="filtersForm">
        ...
    </div>
    ...
</wi-table>

```

####Table columns

| Prop | Types | Default | Description |
|---|---|---|---|
| *field | String | - | datatable field data |
| sortable | Boolean | False | sort datatable column |
| render | Function | - | adds Raw HTML option for datatable field |

*is required

####Pagination

Pagination contains _show items per page_ filter and _pagination_
